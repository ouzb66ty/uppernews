import { render, h, Component } from 'preact';
import { useState, useEffect } from 'preact/hooks';

/** @jsx h */

const Article = (news) => {
    return (
        <div id="article">
            <article class="row">
                <div class="column" id="news">
                    <div id="title">
                        <h2>{ news.news.title }</h2>
                    </div>
                    <div id="description">
                        <p>{ news.news.body }</p>
                    </div>
                    <div id="date">
                        <p>{ news.news.date }</p>
                    </div>
                </div>
            </article>
            <hr/>
        </div>
    )
};

const Section = () => {
    const [news, setNews] = useState([]);

    useEffect(() => {
		fetch(`http://127.0.0.1:8001/news`)
			.then(res => res.json())
			.then(news => setNews(news.result));
    }, []);
    
    return (
        <div id="news" class="row">
            <section class="container clearfix">
                {news.map((result) => (
                    <Article news={result}></Article>
                ))}
            </section>
        </div>
    )
}

const App = () => {
	const [input, setInput] = useState('');

	return (
        <div id="content" class="container">
            <header class="row">
                <div class="column">
                    <img class="float-right" src="/img/uppernews-logo.png" alt="uppernews-logo"/>
                </div>
            </header>
            <Section></Section>
            <footer class="row">
                <div class="column">
                    <p><a href="#">JSON</a> | <a href="#">RSS</a> | <a href="#">Atom</a> | <a href="#">Administration</a> | <a href="mailto:ouzb65ty@protonmail.ch">ouzb65ty@protonmail.ch</a></p>
                </div>
            </footer>
        </div>
	)
}

render(<App />, document.body);